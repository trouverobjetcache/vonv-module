/* Scripts de vonv pour Foundry VTT */

/* *****
   *****
                                  FOUNDRY
   *****
   ***** */

// horloge
horloge = document.createElement("div");
horloge.setAttribute("id", "horloge");
document.body.appendChild(horloge);
let clock = () => {
  let date = new Date();
  let hrs = date.getHours();
  let mins = date.getMinutes();
  let secs = date.getSeconds();
  let period = "AM";
  hrs = hrs < 10 ? "0" + hrs : hrs;
  mins = mins < 10 ? "0" + mins : mins;
  secs = secs < 10 ? "0" + secs : secs;
  //  let time = `${hrs}:${mins}:${secs}:${period}`;
  let time = `⌚ ${hrs}:${mins}`;
  document.getElementById("horloge").innerText = time;
  setTimeout(clock, 2000);
};
clock();

// auto réduit la barre de macros
function collapse(toggleId) {
  let target = document.getElementById(toggleId);
  if (target) {
    target.click()
  }
}


// mise à jour du logo en haut à gauche
var toclogo = document.getElementById("logo");
document.getElementById("logo").remove();

Hooks.on('ready', async function () {

  // activation auto du click gauche pour relacher
  game.settings.set('core', 'leftClickRelease', true);
  game.settings.set('core', 'chatBubbles', false);
  game.settings.set('core', 'chatBubblesPan', false);
  game.settings.set('core', 'showToolclips', false);

  // modif du jet dans dicesonice pour Hey listen fonctionne
  let isDsnActive = game.modules.has("dice-so-nice") ? game.modules.get('dice-so-nice').active : false ;
  if (isDsnActive) game.settings.set('dice-so-nice', 'immediatelyDisplayChatMessages', true);

  // auto réduit la barre de macros
  //collapse("bar-toggle")
})
